<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Login;


class LoginController extends AbstractController
{
    public $api="http://localhost:8000";

    public function index(){
        //check: if not logged in, then
        return $this->redirect($this->generateURL('registerPage'));
    }


    public function registerPage()
    {
        return $this->render('login/register.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }
    public function Register(Request $req){
        $email=$req->request->get('email');
        $name=$req->request->get('name');
        
        $client = new \GuzzleHttp\Client();
        $response=$client->request('POST',$this->api.'/api/register',[
            "form_params"=>[
                'email'=>$email,
                'name'=> $name
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());
        
        $otp="123456";

        if($body->status){
            return $this->getOTP($req);
            return $this->render('login/login.html.twig',['email'=>$email,'otp'=>$otp,'message'=>"OTP was sent to you"]);
        }else
            return $this->render('login/register.html.twig',['message'=>'Registration Failed / try again']);
    }


    public function otp(){
        return $this->render('login/getOTP.html.twig');
    }
    public function getOTP(Request $req){
        $email=$req->request->get('email');
        
        $client = new \GuzzleHttp\Client();
        $response=$client->request('GET',$this->api.'/api/getOTP?email='.$email);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());

        if($body->status)
            return $this->render('login/login.html.twig',['email'=>$email,'otp'=> $body->otp ,'message'=>"OTP was sent to you"]);
        else
            return $this->render('login/getOTP.html.twig',['email'=>$email, 'message'=>'Wrong Email']);
    }


    public function loginPage(){
        return $this->render('login/login.html.twig');
    }
    public function login(Request $req){
        
        $email=$req->request->get('email');
        $otp=$req->request->get('otp');
        
        $client = new \GuzzleHttp\Client();
        $response=$client->request('POST',$this->api.'/api/login',[
            "form_params"=>[
                'email'=>$email,
                'otp'=> $otp
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());

        $login=new Login();
        $login->setEmail($email);
        $login->setToken($body->token);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($login);
        $entityManager->flush();



        if($body->status)
                return $this->redirect($this->generateURL('cats'));
                //return new Response("LOGIN SUCCESSFUL / TOKEN: ".$body->token);
        else
            return $this->render('login/login.html.twig',[
                'email'=>$email,
                'message'=>"Login Failed / try getting another OTP"
            ]);
    }  
}
