<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Login;
use App\Entity\Post;
use App\Service\FileUploader;

class ContentController extends AbstractController
{
    /**
     * @Route("/content", name="content")
     */
    public $api="http://localhost:8000";

    public function index()
    {
        return $this->render('content/index.html.twig', [
            'controller_name' => 'ContentController',
        ]);
    }

    public function listCats(){
        
        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();
                        
        $client = new \GuzzleHttp\Client();
        $response=$client->request('GET',$this->api.'/api/cats?token='.$token);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());
        //return new Response(dd($body));
        
        return $this->render('cats.html.twig',['data'=>$body]);
    }   

    public function newCat(Request $req){
        $label=$req->request->get('catLabel');

        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();
                        
        $client = new \GuzzleHttp\Client();
        $response=$client->request('POST',$this->api.'/api/cat/', [
            'form_params' => [
                'label' => $label,
                'token' => $token
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());
        
        return $this->redirect($this->generateURL('cats'));

    }

    public function deleteCat(Request $req){
        $id=$req->request->get('cat_id');

        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $client = new \GuzzleHttp\Client();
        $response=$client->request('DELETE',$this->api.'/api/cats/'.$id, [
            'form_params' => [
                'token' => $token
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());

        return $this->redirect($this->generateURL('cats'));
    }

    public function updateCat(Request $req){
        $id=$req->request->get('cat_id');
        $label=$req->request->get('label');
        
        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $client = new \GuzzleHttp\Client();
        $response=$client->request('PUT',$this->api.'/api/cats/'.$id, [
            'form_params' => [
                'token' => $token,
                'label' => $label
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody());

        return $this->redirect($this->generateURL('cats'));
    }


    public function listPosts($cid){

        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $client = new \GuzzleHttp\Client();
        $response=$client->request('GET',$this->api.'/api/cats/'.$cid."?token=".$token);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody(),true);
        //return new Response(var_dump($body));
        return $this->render('posts.html.twig',['data'=>$body,'host_api'=>$this->api]);
    }

    public function newPost(Request $req,$cid){
       
        $caption=$req->request->get('caption');
        $file_tmp=$req->files->get('postFile');        

        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $name=bin2hex(random_bytes(20));
        $path=__DIR__."/../../public/storage/";
        move_uploaded_file($file_tmp,$path.$name);

        $client = new \GuzzleHttp\Client();
        $url=$this->api.'/api/posts/';

        $response=$client->request('POST',$url, [
            'form_params' => [
                'token' => $token,
                'caption' =>$caption,
                'cat_id'=>$cid,
                'contents'=>$path.$name
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody(),true);
        
        return $this->redirect($this->generateURL('catPosts',['cid'=> $cid]));
    }

    public function deletePost(Request $req){
        
        $pid=$req->request->get('post_id');
        $cid=$req->request->get('cat_id');
        
        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $client = new \GuzzleHttp\Client();
        $response=$client->request('DELETE',$this->api.'/api/posts/'.$pid, [
            'form_params' => [
                'token' => $token
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody(),true);
        
        return $this->redirect($this->generateURL('catPosts',['cid'=> $cid ]));
    }

    public function editPost(Request $req){
        $caption=$req->request->get('caption');
        $cid=$req->request->get('cat_id');
        $pid=$req->request->get('post_id');

        $login = $this->getDoctrine()->getRepository(Login::class)->findAll();
        $token=end($login)->getToken();

        $client = new \GuzzleHttp\Client();
        $response=$client->request('PUT',$this->api.'/api/posts', [
            'form_params' => [
                'token' => $token,
                'caption' =>$caption,
                'post_id'=>$pid
                //file also
            ]
        ]);

        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'

        $body=json_decode($response->getBody(),true);

        return $this->redirect($this->generateURL('catPosts',['cid'=>$cid]));
    }
}
